require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Led
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Initializes GPIOs
    RPi::GPIO.set_numbering(:bcm)
    RPi::GPIO.set_warnings(false)
    RPi::GPIO.setup(17, as: :output, initialize: :low)

    # Reset GPIOs settings
    at_exit do
      puts '#### Cleaning GPIOs settings...'
      RPi::GPIO.reset
    end
  end
end
