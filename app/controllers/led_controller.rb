class LedController < ApplicationController
  def menu
  end

  def on
    puts '#### Turning LED on...'
    RPi::GPIO.set_high(17)
  end

  def off
    puts '#### Turning LED off...'
    RPi::GPIO.set_low(17)
  end
end
